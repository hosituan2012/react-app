import React from 'react';
import {
  BrowserRouter as Router,
} from 'react-router-dom';
import Root from './components/Root';
import TopBar from './components/TopBar';

const App = () => {
  return (
    <div className="container p-5">
      <div className="row justify-content-sm-center">
        <div className="col-sm-5">
        <React.Suspense fallback={<span>Loading...</span>}>
          <Router>
            <TopBar />
            <Root />
          </Router>
          </React.Suspense>
        </div>
      </div>
    </div>
  )
}

export default App;