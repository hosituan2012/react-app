import React from 'react';
import {
  Redirect,
  Route,
  Switch,
} from 'react-router-dom';

const DashboardRoot = React.lazy(() => import('./Dashboard/Root'));
const ProfilesRoot = React.lazy(() => import('./Profiles/Root'));

const Root = () => {

  return (
    <React.Fragment>
      <Switch>
        <Route exact path="/">
          <Redirect to="/dashboard" />
        </Route>
        <Route exact path="/dashboard" render={() => <DashboardRoot />} />
        <Route exact path="/profiles" render={() => <ProfilesRoot />} />
      </Switch>
    </React.Fragment>
  )
}

export default Root;