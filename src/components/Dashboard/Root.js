import React from 'react';
import {
  Route,
  Switch,
} from 'react-router-dom';

const Dashboard = React.lazy(() => import('./Dashboard'));

const Root = () => {

  return (
    <React.Fragment>
      <Switch>
        <Route exact path="/dashboard" render={() => <Dashboard />} />
      </Switch>
    </React.Fragment>
  )
}

export default Root;