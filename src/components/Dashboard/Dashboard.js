import React from 'react';
import { Link } from 'react-router-dom';
import {
  Button,
  Grid,
  makeStyles,
  Paper,
} from '@material-ui/core';

const useStyles = makeStyles((theme) => ({
  paper: {
    backgroundColor: '#333',
    height: '500px',
    margin: 24,
    padding: 24,
    color: '#fff',
  },
  root: {
    flexGrow: 1,
  },
  paperSmaller: {
    height: 140,
    width: 200,
    backgroundColor: '#424242',
  },
  control: {
    padding: theme.spacing(2),
  },
}));

const Dashboard = (props) => {
  const classes = useStyles();
  return <Paper elevation={0} className={classes.paper}>
    <div style={{ display: 'flex', alignItems: 'center' }}>
      Dashboard
      <Button
        color="primary"
        variant="contained"
        component={Link}
        to="/profiles"
        style={{ marginLeft: 24 }}
      >Click to go to Profile</Button>
    </div>

    <Grid
      container
      className={classes.root}
      spacing={2}
      style={{ marginTop: 24 }}
    >
      <Grid item xs={12}>
        <Grid container justifyContent="center" spacing={2}>
          {[0, 1, 2, 3, 4, 5, 6, 7, 8].map((value) => (
            <Grid key={value} item>
              <Paper className={classes.paperSmaller} />
            </Grid>
          ))}
        </Grid>
      </Grid>
    </Grid>
  </Paper>
}

export default Dashboard;