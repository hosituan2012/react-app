import React from 'react';
import PropTypes from 'prop-types';
import { Button, TextField, withStyles } from '@material-ui/core';
import { Link } from 'react-router-dom';

const styles = theme => ({
  container: {
    display: 'flex',
    flexWrap: 'wrap',
    flexDirection: 'row',
    alignItems: 'center',
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    width: 200,
    margin: 24,
  },
  cssLabel: {
    color : 'white'
  },
  cssOutlinedInput: {
    '&$cssFocused $notchedOutline': {
      borderColor: `${theme.palette.primary.main} !important`,
    },
    color: 'white !important',
  },
  cssFocused: {},
  notchedOutline: {
    borderWidth: '1px',
    borderColor: 'white !important'
  },
});

class ProfileForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      profile: {
        id: '',
        name: '',
      },
    };
  }

  fnSetProfileValue = (id, name) => {
    this.setState({
      profile: {
        id: id,
        name: name,
      }
    });
  }

  fnOnSubmit = () => {
    
  }

  render() {
    const {
      classes,
      onSubmit,
    } = this.props;
    const { profile } = this.state;

    return (
      <React.Fragment>
        <div className={classes.container}>
          <label>Create new profile</label>
          <TextField
            key="id"
            id="outlined-basic"
            label="Id"
            variant="outlined"
            className={classes.textField}
            value={profile.id}
            onChange={e => this.fnSetProfileValue(e.target.value, profile.name)}
            InputLabelProps={{
              classes: {
                root: classes.cssLabel,
                focused: classes.cssFocused,
              },
            }}
            InputProps={{
              classes: {
                root: classes.cssOutlinedInput,
                focused: classes.cssFocused,
                notchedOutline: classes.notchedOutline,
              },
            }}
          />
          <TextField
            id="outlined-basic"
            label="Name"
            variant="outlined"
            className={classes.textField}
            value={profile.name}
            onChange={e => this.fnSetProfileValue(profile.id, e.target.value)}
            InputLabelProps={{
              classes: {
                root: classes.cssLabel,
                focused: classes.cssFocused,
              },
            }}
            InputProps={{
              classes: {
                root: classes.cssOutlinedInput,
                focused: classes.cssFocused,
                notchedOutline: classes.notchedOutline,
              },
            }}
          />
          <Button
            variant="contained"
            color="secondary"
            component={Link}
            to="/profiles"
            style={{ margin: '0 24px' }}
          >Back</Button>
          <Button
            variant="contained"
            color="primary"
            onClick={() =>
              onSubmit
              && profile.name?.length
              && onSubmit({
                ...profile
              })}
          >
            Submit
          </Button>
        </div>
      </React.Fragment>
    )
  }
}

ProfileForm.propTypes = {
  classes: PropTypes.object.isRequired,
  onSubmit: PropTypes.object,
};

export default withStyles(styles)(ProfileForm);