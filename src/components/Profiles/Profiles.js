import React from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import { Button, withStyles } from '@material-ui/core';

const styles = theme => ({
  container: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  itemRow: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 24,
  },
});

class ProfileForm extends React.Component {
  render() {
    const { classes, profiles } = this.props;
    return (
      <React.Fragment>
        <div className={classes.container}>
          <h2>Profile list</h2>
          <ul style={{ listStyleType: 'none' }}>
            {Array.isArray(profiles)
              && profiles
              .map((profile, i) => (
                <div key={`${profile}_${i}`} className={classes.itemRow}>
                  <li>
                    {i}- {profile.id} - {profile.name}
                  </li>
                  <Button
                    variant="contained"
                    color="primary"
                    component={Link}
                    to={`/profiles/${profile.name}`}
                    style={{ marginLeft: 24 }}
                  >Go to detail</Button>
                </div>
              ))}
          </ul>
          <Button
            variant="contained"
            color="primary"
            component={Link}
            to="/profiles/add"
          >Add</Button>
        </div>
      </React.Fragment>
    )
  }
}


ProfileForm.propTypes = {
  classes: PropTypes.object.isRequired,
  profiles: PropTypes.array,
};

export default withStyles(styles)(ProfileForm);
