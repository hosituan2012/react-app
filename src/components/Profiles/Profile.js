import React, { useEffect, useState } from 'react';
import { Link, withRouter } from 'react-router-dom';
import { Button } from '@material-ui/core';

const Profile = ({profiles, match, history}) => {
  const [profile, setProfile] = useState();

  useEffect((profiles, match, history) => {
    const {name} = match.params;

    const foundProfile = profiles.filter(profile => name === profile.name);

    // Check if profile exists
    if (!foundProfile.length) {
      history.push('/profiles');
    }

    // Select profile from list
    setProfile(foundProfile[0]);
  }, [profile]);

  return (
    <div className="card">
      <div className="card-body">
        <h5 className="card-title">
          Hi my name is {profile && profile.name}
        </h5>
        <Button
          variant="contained"
          color="primary"
          component={Link}
          to="/profiles"
        >Back to list profile</Button>
      </div>
    </div>
  )
}

export default withRouter(Profile);