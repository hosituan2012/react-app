import React, { useState } from 'react';
import {
  BrowserRouter as Router,
  Route,
  Switch,
} from 'react-router-dom';

const Profiles = React.lazy(() => import('./Profiles'));
const ProfileForm = React.lazy(() => import('./ProfileForm'));
const Profile = React.lazy(() => import('./Profile'));

const Root = () => {
  const [profiles, setProfiles] = useState([]);

  return (
    <React.Fragment>
      <div style={{ padding: 24 }}>
        <Router>
          <Switch>
            <Route exact path="/profiles" render={() => <Profiles profiles={profiles}/>} />
            <Route path="/profiles/add" render={props => {
              return <ProfileForm onSubmit={profile => {
                setProfiles([...profiles, profile])
                props.history.push('/profiles')
              }} />
            }} />
            <Route exact path="/profiles/:name" render={() => <Profile profiles={profiles} />} />
          </Switch>
        </Router>
      </div>
    </React.Fragment>
  )
}

export default Root;